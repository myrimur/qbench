from abc import ABC, abstractmethod
import re

import qiskit_aer
import qiskit
import cirq
import qsimcirq
import numpy as np
from cirq.contrib.qasm_import import circuit_from_qasm


class SimWrapper(ABC):
    @abstractmethod
    def __init__(self, method: str, device: str, num_threads: int, noise_model: dict | None = None):
        _ = method, device, num_threads, noise_model
        ...

    @property
    @abstractmethod
    def name(self):
        ...

    @abstractmethod
    def circuit_from_qasm(self, qasm: str):
        ...

    @abstractmethod
    def run(self, circuit, shots: int):
        ...

    @staticmethod
    @abstractmethod
    def get_counts(result) -> dict:
        ...


class QiskitSimWrapper(SimWrapper):
    def __init__(self,
                 method: str = 'statevector',
                 device: str = 'cpu',
                 num_threads: int = 1,
                 noise_model: dict | None = None):
        if method not in qiskit_aer.AerSimulator().available_methods():
            raise ValueError(f'Unknown method: {method}')

        match device:
            case 'cpu':
                kwargs = dict(device='CPU', max_parallel_threads=num_threads)
            case 'gpu':
                kwargs = dict(device='GPU')
            case _:
                raise ValueError(f'Unknown device: {device}')

        self._sim = qiskit_aer.AerSimulator(method=method, **kwargs)

    @property
    def name(self):
        return f'qiskit_{self._sim.options["method"]}'

    def circuit_from_qasm(self, qasm: str) -> qiskit.QuantumCircuit:
        circuit = qiskit.QuantumCircuit.from_qasm_str(qasm)

        match self._sim.options['method']:
            case 'extended_stabilizer':
                circuit.save_statevector()
            case 'unitary' | 'superop':
                circuit.save_state()
            case _:
                circuit.measure_all()

        return qiskit.transpile(circuit, self._sim, optimization_level=3)

    def run(self, circuit: qiskit.QuantumCircuit, shots: int = 1000):
        result = self._sim.run(circuit, shots=shots).result()

        errors = ['Insufficient memory to run circuit', 'Cannot allocate memory', 'std::bad_alloc',
                  'requires more memory than max_memory_mb']
        if any(error in result.status for error in errors):
            raise MemoryError(result.status)

        return result

    @staticmethod
    def get_counts(result):
        return result.get_counts()


class CirqBaseWrapper(SimWrapper):
    def __init__(self, sim, name: str):
        self._sim = sim
        self._name = name

    @property
    def name(self):
        return self._name

    def circuit_from_qasm(self, qasm: str):
        circuit = circuit_from_qasm(qasm=qasm)
        return circuit

    def run(self, circuit: cirq.Circuit, shots: int = 1000):
        return self._sim.simulate(circuit)

    @staticmethod
    def get_counts(result):
        cirq_measurements = result.measurements
        num_measurements = cirq_measurements[list(cirq_measurements.keys())[0]].shape[0]

        # Concatenate measurements into bitstrings
        bitstrings = []
        for i in range(num_measurements):
            bitstring = ''.join(
                str(cirq_measurements[qubit][i, 0]) for qubit in sorted(cirq_measurements.keys(), reverse=True))
            bitstrings.append(bitstring)

        # Count occurrences of each bitstring
        qiskit_measurements = {}
        for bitstring in bitstrings:
            if bitstring in qiskit_measurements:
                qiskit_measurements[bitstring] += 1
            else:
                qiskit_measurements[bitstring] = 1

        return qiskit_measurements


class CirqSimWrapper(CirqBaseWrapper, SimWrapper):
    def __init__(self,
                 method: str = 'statevector',
                 device: str = 'cpu',
                 num_threads: int = 1,
                 noise_model: dict | None = None):
        match method:
            case 'statevector':
                sim = cirq.Simulator()
            case 'density_matrix':
                sim = cirq.DensityMatrixSimulator()
            case _:
                raise ValueError(f'Unknown method: {method}')

        if device != 'cpu':
            raise NotImplementedError(f'Unsupported device: {device}')

        if num_threads != 1:
            raise NotImplementedError('Multi-threading is not supported for Cirq simulators')

        super().__init__(sim, f'cirq_{method}')


class QsimSimWrapper(CirqBaseWrapper, SimWrapper):
    def __init__(self,
                 method: str = 'statevector',
                 device: str = 'cpu',
                 num_threads: int = 1,
                 noise_model: dict | None = None):
        match device:
            case 'cpu':
                options = qsimcirq.QSimOptions(cpu_threads=num_threads)
            case 'gpu':
                options = qsimcirq.QSimOptions(use_gpu=True)
            case _:
                raise ValueError(f'Unknown device: {device}')

        match method:
            case 'statevector':
                sim = qsimcirq.QSimSimulator(options)
            case 'hybrid':
                # TODO: Implement hybrid method
                raise NotImplementedError()
            case _:
                raise ValueError(f'Unknown method: {method}')

        super().__init__(sim, f'qsim_{method}')
