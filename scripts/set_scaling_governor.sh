#!/bin/bash

# Check if the script is run with root privileges
if [ "$(id -u)" -ne 0 ]; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Check the argument and set the scaling governor mode
case "$1" in
    performance|powersave|ondemand)
        echo "Setting CPU scaling governor to $1..."
        for i in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
        do
            echo $1 > $i
        done
        ;;
    *)
        echo "Usage: $0 performance | powersave | ondemand"
        exit 1
        ;;
esac

echo "Done."
