#!/bin/bash

# Check if the script is run with root privileges
if [ "$(id -u)" -ne 0 ]; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Check if the correct number of arguments is passed
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 --disable | --enable <start_cpu> <end_cpu>"
    exit 1
fi

action=$1
start_cpu=$2
end_cpu=$3

# Validate the CPU range
if ! [[ "$start_cpu" =~ ^[0-9]+$ ]] || ! [[ "$end_cpu" =~ ^[0-9]+$ ]]; then
    echo "Error: CPU range must be integer values"
    exit 1
fi

if [ "$start_cpu" -gt "$end_cpu" ]; then
    echo "Error: Start CPU must be less than or equal to end CPU"
    exit 1
fi

# Set the CPU online status based on the action
case "$action" in
    --disable)
        echo "Disabling CPUs $start_cpu-$end_cpu..."
        for i in $(seq $start_cpu $end_cpu)
        do
            echo 0 > /sys/devices/system/cpu/cpu$i/online
        done
        ;;
    --enable)
        echo "Enabling CPUs $start_cpu-$end_cpu..."
        for i in $(seq $start_cpu $end_cpu)
        do
            echo 1 > /sys/devices/system/cpu/cpu$i/online
        done
        ;;
    *)
        echo "Usage: $0 --disable | --enable <start_cpu> <end_cpu>"
        exit 1
        ;;
esac

echo "Done."
