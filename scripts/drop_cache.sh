#!/bin/bash
echo "Dropping disk cache from RAM..."
sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches'
echo "Cache dropped successfully."
