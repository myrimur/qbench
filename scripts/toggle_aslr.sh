#!/bin/bash

# Check if the script is run with root privileges
if [ "$(id -u)" -ne 0 ]; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Check the argument and perform the corresponding action
case "$1" in
    --enable)
        echo "Enabling ASLR..."
        echo 2 > /proc/sys/kernel/randomize_va_space
        ;;
    --disable)
        echo "Disabling ASLR..."
        echo 0 > /proc/sys/kernel/randomize_va_space
        ;;
    *)
        echo "Usage: $0 --enable | --disable"
        exit 1
        ;;
esac

echo "Done."
