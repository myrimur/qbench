#!/bin/bash

# Check if the script is run with root privileges
if [ "$(id -u)" -ne 0 ]; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Check the argument and set the Turbo Boost mode
case "$1" in
    --enable)
        echo "Enabling Turbo Boost..."
        echo 0 > /sys/devices/system/cpu/intel_pstate/no_turbo
        ;;
    --disable)
        echo "Disabling Turbo Boost..."
        echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo
        ;;
    *)
        echo "Usage: $0 --enable | --disable"
        exit 1
        ;;
esac

echo "Done."
