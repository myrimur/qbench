# QBench

A framework for benchmarking quantum computer simulators.

[//]: # (## Requirements)

[//]: # (Our benchmark include the following software packages:)

[//]: # (- Qiskit)

[//]: # (- Cirq)

[//]: # (- QSim)

## Installation

```bash
git clone https://gitlab.com/myrimur/qbench
cd qbench
pip install -r requirements.txt
```

If you want to run GPU simulations with Qiskit, you have to additionally install this package:
```bash
pip install qiskit-aer-gpu
```

The package above is for CUDA® 12, so if your system has CUDA® 11 installed, install separate package:
```bash
pip install qiskit-aer-gpu-cu11
```

**Note**: This package is only available on x86_64 Linux. For other platforms that have CUDA support, you will have to build
from [source](https://github.com/Qiskit/qiskit-aer). 

## Usage

```bash
python benchmark.py <config_file>
```

For precise benchmarking, we included scripts for tweaking the system settings to reduce noise. We highly recommend
following these steps before benchmarking:

- Setting `scaling_governor` to performance:
    ```bash
    ./scripts/set_scaling_governor.sh performance
    ```

- Disabling turbo mode on Intel CPUs:
    ```bash
    ./scripts/toggle_turbo_boost.sh --disable
    ```

- Disabling the SMT pair of the CPUs used for the benchmark:
    ```bash
    ./scripts/toggle_cpus.sh --disable <start_cpu> <end_cpu>
    ```

- Disable address space randomization:
    ```bash
    ./scripts/set_scaling_governor.sh performance
    ```

- Dropping the disk cache from RAM:
    ```bash
    ./scripts/drop_cache.sh 
    ```
- Setting the benchmark program’s task affinity to fixed CPUs and the benchmark program’s priority to max
    ```bash
    taskset -c <start_cpu>,<end_cpu> nice -n -20 python benchmark.py <config_file>
    ```

## Parameter Configuration

Experiment configuration in our framework is done using YAML files. In our benchmarks,
there are separate configuration files for each experiment we run. Here we show an example experiment
configuration:

```yaml
experiment-name: example-experiment
results-dir: results

circuit: qv
sims: [ qiskit_statevector, qiskit_density_matrix,
        qiskit_matrix_product_state, qiskit_extended_stabilizer,
        qiskit_unitary, qiskit_superop, qsim_statevector,
        cirq_statevector, cirq_density_matrix ]

device: cpu
num-threads: 1

trials: 3
max-iters: 1.e+9
min-iters-time-seconds: 1
shots: 1000

min-qubits: 6
timeout-seconds: 600
max-memory-mb: 28_672
```

Some of these parameters have default values and can be excluded in the configuration file. If the experiment name is
not explicitly set, we generate it using a timestamp, for example, `20240404-173759`, and save the results in the
corresponding subdirectory. In our repository, all the results of our benchmarks are available in the default `results`
directory. The device defaults to CPU with one thread. Trials are defaulted to three, and until the simulation takes
more than one second, it is repeated in a loop, but no more than 10<sup>9</sup> iterations. By default, every simulation
runs
with 1000 shots. The memory is limited to 28 gigabytes, and the timeout is 10 minutes. We used these values in our
benchmarks, and they are default in the framework.

## Available Simulators

| Library | Method               | Noise         | Accuracy    | MT | GPU           | MPI |
|---------|----------------------|---------------|-------------|----|---------------|-----|
| Qiskit  | Statevector          | +             | Exact       | +  | +             | +   |
| Qiskit  | Matrix Product State | +             | Approximate | +  | -             | -   |
| Qiskit  | Density Matrix       | +             | Exact       | +  | +             | +   |
| Qiskit  | Tensor Network       | +             | Approximate | -  | +<sup>a</sup> | -   |
| Qiskit  | Stabilizer           | +<sup>b</sup> | Approximate | +  | -             | -   |
| Qiskit  | Extended Stabilizer  | +<sup>b</sup> | Approximate | +  | -             | -   |
| Qiskit  | Unitary              | +             | Exact       | +  | +             | +   |
| Qiskit  | Superoperator        | +             | Exact       | +  | -             | -   |
| Cirq    | Statevector          | +<sup>c</sup> | Exact       | -  | -             | -   |
| Cirq    | Density Matrix       | +             | Exact       | -  | -             | -   |
| QSim    | Statevector          | +             | Exact       | +  | +             | -   |

<sup>a</sup> GPU only.<br />
<sup>b</sup> Limited to Clifford errors.<br />
<sup>c</sup> Must preserve state purity.<br />

## Data and Logging

For each successful benchmarking run, we log the numeric results in the experiment's subdirectory. Numeric results are
saved in the `exec_time.csv`, `mem_usage.csv`, `max_qubits.csv` files. We also include plots in PDF files to visually
inspect the data. Apart from the numeric results, we also log information during the benchmarking process. The logs are
stored in the `log.txt` file. A typical directory structure looks like this:

```
results/
└── 20240404-173759/
    ├── exec_time.csv
    ├── exec_time.pdf
    ├── log.txt
    ├── max_qubits.csv
    ├── max_qubits.pdf
    ├── mem_usage.csv
    └── mem_usage.pdf
```

## Experiments and Results

### System specifications

All of our tests were conducted on a user-grade laptop. Its technical characteristics are provided in the table:

| Characteristic       | Value                  |
|----------------------|------------------------|
| CPU Model            | Intel® Core™ i7-10750H |
| CPU Architecture     | x86\_64                |
| Total Cores          | 6                      |
| SMT Threads          | 12                     |
| CPU Base Frequency   | 2.6 GHz                |
| CPU Max Frequency    | 5.0 GHz                |
| L1 Instruction Cache | 6 x 32 KB              |
| L1 Data Cache        | 6 x 32 KB              |
| L2 Cache             | 6 x 256 KB             |
| L3 Cache             | 12 MB                  |
| RAM                  | 32 GB                  |
| RAM Type             | DDR4 SDRAM             |
| RAM Frequency        | 3.2 GHz                |
| OS Name              | Ubuntu 22.04.4 LTS     |
| OS Type              | 64-bit                 |
| OS Kernel            | Linux 5.19             |

<div align="center">
<h3 align="center">Single-Thread Execution Time</h3>
</div>

![laptop-cpu-1thr-exec-time-ratio](results/laptop-cpu-1thr-exec-time-ratio.png)

<div align="center">
<h3 align="center">Single-Thread Memory Usage</h3>
</div>

![laptop-cpu-1thr-exec-mem-usage-ratio](results/laptop-cpu-1thr-mem-usage-ratio.png)

<div align="center">
<h3 align="center">Multi-Thread Execution Time</h3>
</div>

![laptop-cpu-4thr-exec-time-ratio](results/laptop-cpu-4thr-exec-time-ratio.png)

<div align="center">
<h3 align="center">Parallelization Speedup</h3>
</div>

![laptop-cpu-heat](results/laptop-cpu-heat.png)

## Credits

Circuit code for Hamiltonian simulation and QFT was taken
from [Application-Oriented Performance Benchmarks](https://github.com/SRI-International/QC-App-Oriented-Benchmarks).