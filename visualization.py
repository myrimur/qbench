import pandas as pd
import matplotlib.pylab as plt
import seaborn as sns


def _plot_generic(data: pd.DataFrame, y_label: str, path: str | None):
    sns.set_theme(palette="muted")
    ax = sns.lineplot(data=data.T, dashes=True, markers=True)
    plt.xlabel("Qubits")
    plt.ylabel(y_label)
    plt.yscale("log")
    qubits = data.columns.values
    ax.set_xticks(range(min(qubits), max(qubits) + 1, 2))
    plt.grid(True, which="both", ls="--")
    sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))
    if path is not None:
        plt.savefig(path, bbox_inches='tight')
    plt.show()


def plot_exec_time(data: pd.DataFrame, path: str | None = None):
    _plot_generic(data, "Time (s)", path)


def plot_mem_usage(data: pd.DataFrame, path: str | None = None):
    _plot_generic(data, "Memory (MB)", path)


def plot_max_qubits(data: pd.DataFrame, path: str | None = None):
    sns.set_theme(palette="muted")
    ax = sns.barplot(data=data, x='Qubits', y='Simulator', dodge=False, hue='Limitation')
    plt.ylabel("Simulator")
    plt.xlabel("Qubits")
    ax.set_yticks(range(len(data)))
    qubits = data.Qubits
    ax.set_xticks(range(0, max(qubits) + 1, 2))
    ax.set_yticklabels(data.index)
    plt.grid(True, which="both", ls="--")
    sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))
    if path is not None:
        plt.savefig(path, bbox_inches='tight')
    plt.show()


def _save_generic_plot(data: pd.DataFrame, y_label: str, path: str):
    sns.set_theme(palette="muted")
    ax = sns.lineplot(data=data.T, dashes=True, markers=True)
    plt.xlabel("Qubits")
    plt.ylabel(y_label)
    plt.yscale("log")
    qubits = data.columns.values
    ax.set_xticks(range(min(qubits), max(qubits) + 1, 2))
    plt.grid(True, which="both", ls="--")
    sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))
    plt.savefig(path, bbox_inches='tight')
    plt.close()


def save_exec_time_plot(data: pd.DataFrame, path: str):
    _save_generic_plot(data, "Time (s)", path)


def save_mem_usage_plot(data: pd.DataFrame, path: str):
    _save_generic_plot(data, "Memory (MB)", path)


def save_max_qubits_plot(data: pd.DataFrame, path: str):
    sns.set_theme(palette="muted")
    ax = sns.barplot(data=data, x='Qubits', y='Simulator', dodge=False, hue='Limitation')
    plt.ylabel("Simulator")
    plt.xlabel("Qubits")
    ax.set_yticks(range(len(data)))
    qubits = data.Qubits
    ax.set_xticks(range(0, max(qubits) + 1, 2))
    ax.set_yticklabels(data.index)
    plt.grid(True, which="both", ls="--")
    sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))
    plt.savefig(path, bbox_inches='tight')
    plt.close()
