import qiskit
import qiskit.qasm2
from qiskit.circuit.library import QuantumVolume as QuantumVolumeCircuit
from qiskit.transpiler.passes import RemoveBarriers
from qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit
import math
import numpy as np


def qiskit_to_base_set_qasm(circuit: qiskit.QuantumCircuit) -> str:
    remove_barriers = qiskit.transpiler.passes.RemoveBarriers()

    transpiled_circuit = qiskit.transpile(circuit, basis_gates=['rx', 'ry', 'rz', 'cx'], optimization_level=3)
    circuit_without_barriers = remove_barriers(transpiled_circuit)
    qasm = qiskit.qasm2.dumps(circuit_without_barriers)
    return qasm


def qft_gate(input_size):
    """
    https://github.com/SRI-International/QC-App-Oriented-Benchmarks/blob/14ea7a4525f1588334aefd4d82633ab18683b5e0/quantum-fourier-transform/qiskit/qft_benchmark.py#L132
    """
    qr = QuantumRegister(input_size);
    qc = QuantumCircuit(qr, name="qft")

    # Generate multiple groups of diminishing angle CRZs and H gate
    for i_qubit in range(0, input_size):

        # start laying out gates from highest order qubit (the hidx)
        hidx = input_size - i_qubit - 1

        # if not the highest order qubit, add multiple controlled RZs of decreasing angle
        if hidx < input_size - 1:
            num_crzs = i_qubit
            for j in range(0, num_crzs):
                divisor = 2 ** (num_crzs - j)
                qc.crz(math.pi / divisor, qr[hidx], qr[input_size - j - 1])

        # followed by an H gate (applied to all qubits)
        qc.h(qr[hidx])

        qc.barrier()

    return qc


def inv_qft_gate(input_size):
    """
    https://github.com/SRI-International/QC-App-Oriented-Benchmarks/blob/14ea7a4525f1588334aefd4d82633ab18683b5e0/quantum-fourier-transform/qiskit/qft_benchmark.py#L165
    """
    qr = QuantumRegister(input_size);
    qc = QuantumCircuit(qr, name="inv_qft")

    # Generate multiple groups of diminishing angle CRZs and H gate
    for i_qubit in reversed(range(0, input_size)):

        # start laying out gates from highest order qubit (the hidx)
        hidx = input_size - i_qubit - 1

        # precede with an H gate (applied to all qubits)
        qc.h(qr[hidx])

        # if not the highest order qubit, add multiple controlled RZs of decreasing angle
        if hidx < input_size - 1:
            num_crzs = i_qubit
            for j in reversed(range(0, num_crzs)):
                divisor = 2 ** (num_crzs - j)
                qc.crz(-math.pi / divisor, qr[hidx], qr[input_size - j - 1])

        qc.barrier()

    return qc


def make_qft_with_expected_dist(num_qubits: int,
                                secret_int: int | None = None,
                                add_one: bool = True,
                                follow_by_inv: bool = True) -> tuple[str, dict[str, float]]:
    """
    https://github.com/SRI-International/QC-App-Oriented-Benchmarks/blob/14ea7a4525f1588334aefd4d82633ab18683b5e0/quantum-fourier-transform/qiskit/qft_benchmark.py#L33C5-L33C28
    """
    assert num_qubits > 0
    assert num_qubits <= 64

    # Size of input is one less than available qubits
    input_size = num_qubits

    if secret_int is None:
        secret_int = np.random.randint(0, 2 ** input_size, dtype=np.uint64)
    else:
        secret_int = np.uint64(secret_int)

    secret_int_plus_one = secret_int + np.uint64(1)
    if input_size != 64:
        secret_int_plus_one %= np.uint64(2 ** input_size)

    expected_dist = {('{0:0' + str(input_size) + 'b}').format(secret_int_plus_one): 1.}

    # allocate qubits
    qr = QuantumRegister(num_qubits);
    cr = ClassicalRegister(num_qubits)
    qc = QuantumCircuit(qr, cr, name=f"qft-{num_qubits}-{secret_int}")

    # Perform X on each qubit that matches a bit in secret string
    s = ('{0:0' + str(input_size) + 'b}').format(secret_int)
    for i_qubit in range(input_size):
        if s[input_size - 1 - i_qubit] == '1':
            qc.x(qr[i_qubit])

    qc.barrier()

    # perform QFT on the input
    qc.append(qft_gate(input_size).to_instruction(), qr)

    qc.barrier()

    if add_one:
        # some compilers recognize the QFT and IQFT in series and collapse them to identity;
        # perform a set of rotations to add one to the secret_int to avoid this collapse
        for i_q in range(0, num_qubits):
            divisor = 2 ** (i_q)
            qc.rz(1 * math.pi / divisor, qr[i_q])

        qc.barrier()

    if follow_by_inv:
        # to revert back to initial state, apply inverse QFT
        qc.append(inv_qft_gate(input_size).to_instruction(), qr)

        qc.barrier()

    return qiskit_to_base_set_qasm(qc), expected_dist


def make_qft(num_qubits: int,
             secret_int: int | None = None,
             add_one: bool = True,
             follow_by_inv: bool = True) -> str:
    qasm, _ = make_qft_with_expected_dist(num_qubits, secret_int, add_one, follow_by_inv)
    return qasm


# Precomputed data for default parameters
H_X = [-0.3841300947500583, 0.03878295861060144, 0.5365953230759468, 0.5784414723417195, 0.7411241249516469,
       -0.6241572167558622, -0.4609895087661635, -0.007615718024277518, 0.4782434942274214, -0.6100960167167189,
       -0.6405095065481761, 0.07765251715030663, -0.4344687907780449, 0.5218797389241974, -0.46061411670810526,
       -0.41296098137198967, 0.9248110232255489, 0.8264286523422202, 0.1363496908115518, -0.029015291992941616,
       0.3074087412971702, 0.07325879705874927, 0.9914777948008391, -0.7160350722371267, 0.9506832731474528,
       0.23911494800528166, 0.4409724154099488, -0.40987285724885636, 0.20026393114016572, 0.5012709441157315,
       0.28273234928097457, 0.8465989190360859, 0.7774604828054057, 0.2893427050334032, -0.4087151798764068,
       0.3651874256643728, -0.4460054640434348, -0.7471400926292047, 0.13983454220929192, -0.18188101148750735,
       0.12081029457007308, 0.7086868405963342, -0.8065622056703374, -0.29131425372778175, 0.9472435425232026,
       0.8439322550877246, 0.9373329768311951, -0.8980872301386897, 0.7671117602853732, 0.5399493734173173,
       -0.5207691406177182, -0.7923704994133167, -0.6274053573740819, 0.5054405311178245, 0.9057944518252579,
       0.5811640480472717, -0.6267815045950822, 0.024093876514304835, -0.8371018242326111, 0.04647954857405123,
       -0.7322817244721747, -0.6027509735183574, -0.7836093805503312, 0.26545045118303445]

H_Z = [0.13788044023849122, -0.8680715448211151, 0.7762213398005142, 0.8593163543481379, 0.35938408275225986,
       0.08855163284966783, -0.6243013156839929, 0.2365269243700283, -0.2517444632292354, 0.867164336543846,
       0.296282404856135, 0.3435823547580954, 0.8631471356486622, 0.3399761708363991, -0.5357509772547095,
       -0.4883312979088732, -0.5500163526392827, -0.8385986892423059, -0.6319995154933682, 0.44989873651812795,
       0.9978612715947381, -0.7136597695220406, -0.5585570010105623, -0.4430940963002987, 0.4941155190144264,
       0.9609639719269489, 0.5559901833438659, 0.39028565515510283, 0.8969098837145426, -0.8443034756454515,
       -0.5798694539672691, 0.3309577017424028, 0.5987506507749656, 0.2766837078970843, -0.9334020058114207,
       -0.42961426102187983, -0.8296236571981324, 0.7505406680095144, 0.9094424165255053, 0.4568253262413007,
       -0.9494669644389169, 0.511221293080349, 0.7836130156121213, -0.4695796398985512, 0.8627338496138965,
       -0.18485654166256293, -0.16966247047622107, -0.604158184903371, 0.9713200630773993, -0.37341079483002027,
       -0.6419480501289898, -0.9770100334018375, 0.6032919486600323, 0.007758295857174691, -0.06374285415521541,
       0.5588979017503308, -0.4330085117934137, -0.7832711382064108, 0.7635000128475307, 0.8814394048329894,
       0.203592130143315, 0.28867463241776825, -0.0011212816212540222, 0.4640859607178969]


def make_hamsim(n_spins: int,
                K: int = 3,
                t: float = 0.01,
                w: int = 10,
                h_x: list[int] | None = None,
                h_z: list[int] | None = None,
                _use_XX_YY_ZZ_gates: bool = False):
    if h_x is None:
        h_x = H_X[:n_spins]
    if h_z is None:
        h_z = H_Z[:n_spins]

    num_qubits = n_spins
    secret_int = f"{K}-{t}"

    # allocate qubits
    qr = QuantumRegister(n_spins);
    cr = ClassicalRegister(n_spins)
    qc = QuantumCircuit(qr, cr, name=f"hamsim-{num_qubits}-{secret_int}")
    tau = t / K

    # start with initial state of 1010101...
    for k in range(0, n_spins, 2):
        qc.x(qr[k])
    qc.barrier()

    # loop over each trotter step, adding gates to the circuit defining the hamiltonian
    for k in range(K):

        # the Pauli spin vector product
        [qc.rx(2 * tau * w * h_x[i], qr[i]) for i in range(n_spins)]
        [qc.rz(2 * tau * w * h_z[i], qr[i]) for i in range(n_spins)]
        qc.barrier()

        # Basic implementation of exp(i * t * (XX + YY + ZZ))
        if _use_XX_YY_ZZ_gates:

            # XX operator on each pair of qubits in linear chain
            for j in range(2):
                for i in range(j % 2, n_spins - 1, 2):
                    qc.append(xx_gate(tau).to_instruction(), [qr[i], qr[(i + 1) % n_spins]])

            # YY operator on each pair of qubits in linear chain
            for j in range(2):
                for i in range(j % 2, n_spins - 1, 2):
                    qc.append(yy_gate(tau).to_instruction(), [qr[i], qr[(i + 1) % n_spins]])

            # ZZ operation on each pair of qubits in linear chain
            for j in range(2):
                for i in range(j % 2, n_spins - 1, 2):
                    qc.append(zz_gate(tau).to_instruction(), [qr[i], qr[(i + 1) % n_spins]])

        # Use an optimal XXYYZZ combined operator
        # See equation 1 and Figure 6 in https://arxiv.org/pdf/quant-ph/0308006.pdf
        else:

            # optimized XX + YY + ZZ operator on each pair of qubits in linear chain
            for j in range(2):
                for i in range(j % 2, n_spins - 1, 2):
                    qc.append(xxyyzz_opt_gate(tau).to_instruction(), [qr[i], qr[(i + 1) % n_spins]])

        qc.barrier()

    return qiskit_to_base_set_qasm(qc)


############### XX, YY, ZZ Gate Implementations

# Simple XX gate on q0 and q1 with angle 'tau'
def xx_gate(tau):
    qr = QuantumRegister(2);
    qc = QuantumCircuit(qr, name="xx_gate")
    qc.h(qr[0])
    qc.h(qr[1])
    qc.cx(qr[0], qr[1])
    qc.rz(3.1416 * tau, qr[1])
    qc.cx(qr[0], qr[1])
    qc.h(qr[0])
    qc.h(qr[1])

    # save circuit example for display
    global XX_
    XX_ = qc

    return qc


# Simple YY gate on q0 and q1 with angle 'tau'
def yy_gate(tau):
    qr = QuantumRegister(2);
    qc = QuantumCircuit(qr, name="yy_gate")
    qc.s(qr[0])
    qc.s(qr[1])
    qc.h(qr[0])
    qc.h(qr[1])
    qc.cx(qr[0], qr[1])
    qc.rz(3.1416 * tau, qr[1])
    qc.cx(qr[0], qr[1])
    qc.h(qr[0])
    qc.h(qr[1])
    qc.sdg(qr[0])
    qc.sdg(qr[1])

    # save circuit example for display
    global YY_
    YY_ = qc

    return qc


# Simple ZZ gate on q0 and q1 with angle 'tau'
def zz_gate(tau):
    qr = QuantumRegister(2);
    qc = QuantumCircuit(qr, name="zz_gate")
    qc.cx(qr[0], qr[1])
    qc.rz(3.1416 * tau, qr[1])
    qc.cx(qr[0], qr[1])

    # save circuit example for display
    global ZZ_
    ZZ_ = qc

    return qc


# Optimal combined XXYYZZ gate (with double coupling) on q0 and q1 with angle 'tau'
def xxyyzz_opt_gate(tau):
    alpha = tau;
    beta = tau;
    gamma = tau
    qr = QuantumRegister(2);
    qc = QuantumCircuit(qr, name="xxyyzz_opt")
    qc.rz(3.1416 / 2, qr[1])
    qc.cx(qr[1], qr[0])
    qc.rz(3.1416 * gamma - 3.1416 / 2, qr[0])
    qc.ry(3.1416 / 2 - 3.1416 * alpha, qr[1])
    qc.cx(qr[0], qr[1])
    qc.ry(3.1416 * beta - 3.1416 / 2, qr[1])
    qc.cx(qr[1], qr[0])
    qc.rz(-3.1416 / 2, qr[0])

    # save circuit example for display
    global XXYYZZ_
    XXYYZZ_ = qc

    return qc


def make_qv(num_qubits: int) -> str:
    qv_circuit = QuantumVolumeCircuit(num_qubits)
    return qiskit_to_base_set_qasm(qv_circuit)
