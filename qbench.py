from collections.abc import Collection
from typing import Callable
import timeit
import gc
import logging
from datetime import datetime
import multiprocessing
import memory_profiler
import numpy as np
import pandas as pd
from qiskit.transpiler import CircuitTooWideForTarget
from tqdm import tqdm
from wrappers import SimWrapper

# Monkey patching timeit to return the result of the statement
timeit.template = """
def inner(_it, _timer{init}):
    {setup}
    _t0 = _timer()
    for _i in _it:
        retval = {stmt}
    _t1 = _timer()
    return _t1 - _t0, retval
"""


def _benchmark_time(circ, sim: SimWrapper, shots: int, iters: int, queue: multiprocessing.Queue) -> None:
    gc.collect()
    try:
        # Passing as a lambda is possible but increases the overhead
        exec_time, _ = timeit.timeit(stmt='sim.run(circ, shots)', number=iters, globals=locals())
        queue.put(exec_time)

    except (MemoryError, CircuitTooWideForTarget) as e:
        queue.put(e)


def benchmark(sims: Collection[SimWrapper],
              circuit_factory: Callable[[int], str],
              min_qubits: int = 6,
              shots: int = 1000,
              trials: int = 3,
              timeout_seconds: int = 600,
              max_memory_mb: int = 28_672,
              min_iters_time_seconds: int = 1,
              max_iters: int = 1e9,
              logger: logging.Logger | None = None) -> tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    exec_time_table = []
    mem_usage_table = []

    max_qubits = []
    limitations = []

    queue = multiprocessing.Queue()

    if logger is not None:
        logger.info('Starting benchmark')

    for sim in (pbar := tqdm(sims)):
        exec_time_row = []
        mem_usage_row = []

        num_qubits = min_qubits
        while True:
            exec_time_reps = []
            mem_usage_reps = []

            iters = 1
            for rep in range(trials):
                qasm = circuit_factory(num_qubits)

                try:
                    circ = sim.circuit_from_qasm(qasm)

                    while True:
                        info = f'{sim.name} | {num_qubits} qbit | trial {rep + 1} | {iters:.0e} iters'
                        pbar.set_description(f'{datetime.now().strftime("%H:%M:%S")} | ' + info)

                        if logger is not None:
                            logger.info(info)

                        process = multiprocessing.Process(target=_benchmark_time, args=(circ, sim, shots, iters, queue))
                        process.start()

                        mem_usage = -1
                        start_time = timeit.default_timer()
                        while timeit.default_timer() < start_time + timeout_seconds and process.is_alive():
                            ret = memory_profiler.memory_usage(process.pid, timeout=.1, interval=.1, max_usage=True,
                                                               include_children=True)
                            if ret > max_memory_mb:
                                process.terminate()
                                raise MemoryError("Process memory usage exceeds memory limit")

                            mem_usage = max(mem_usage, ret)

                        if process.is_alive():
                            process.terminate()
                            raise TimeoutError(f'Timed out after {timeout_seconds}s')

                        assert queue.qsize() == 1

                        ret = queue.get()
                        if isinstance(ret, Exception):
                            raise ret

                        total_time = ret
                        exec_time = ret / iters

                        if total_time > min_iters_time_seconds or iters > max_iters:
                            break
                        else:
                            iters *= 10

                except (TimeoutError, MemoryError, CircuitTooWideForTarget) as e:
                    if logger is not None:
                        logger.info(f'{sim.name} | {num_qubits} qbit | {e}')

                    exec_time_reps = [np.nan]
                    max_qubits.append(num_qubits - 1)

                    match e:
                        case TimeoutError():
                            limitations.append('Time')
                        case MemoryError() | CircuitTooWideForTarget():
                            limitations.append('Memory')

                    break

                exec_time_reps.append(exec_time)
                mem_usage_reps.append(mem_usage)

            if exec_time_reps == [np.nan]:
                break

            exec_time_row.append(np.min(exec_time_reps))
            mem_usage_row.append(np.max(mem_usage_reps))

            num_qubits += 1

        exec_time_table.append(exec_time_row)
        mem_usage_table.append(mem_usage_row)

    if logger is not None:
        logger.info('Benchmark finished')

    max_row_len = 0
    for row in exec_time_table:
        if len(row) > max_row_len:
            max_row_len = len(row)

    for table in [exec_time_table, mem_usage_table]:
        for row in table:
            row += [np.nan] * (max_row_len - len(row))

    sim_names = [sim.name for sim in sims]
    qubits = list(range(min_qubits, min_qubits + max_row_len))

    exec_time_df = pd.DataFrame(exec_time_table, columns=qubits, index=sim_names)
    mem_usage_df = pd.DataFrame(mem_usage_table, columns=qubits, index=sim_names)
    max_qubits_df = pd.DataFrame({'Qubits': max_qubits, 'Limitation': limitations}, index=sim_names)

    for df in [exec_time_df, mem_usage_df, max_qubits_df]:
        df.index.name = 'Simulator'

    return exec_time_df, mem_usage_df, max_qubits_df
