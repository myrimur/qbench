import os
import sys
import argparse
import shutil
import logging
from datetime import datetime
import yaml
import random
import numpy as np
from qbench import benchmark
import visualization  # noqa # pylint: disable=unused-import
import circuits  # noqa # pylint: disable=unused-import
import wrappers  # noqa # pylint: disable=unused-import

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Benchmark with a given config.")
    parser.add_argument('config_file', type=str, help='Path to the configuration file')

    args = parser.parse_args()

    if args.config_file is None:
        parser.print_help(sys.stderr)
        sys.exit(1)

    with open(args.config_file, 'r') as file:
        config = yaml.safe_load(file)

    seed = config.get('seed', 111)
    random.seed(seed)
    np.random.seed(seed)

    circuit = config['circuit']
    circuit_factory = eval(f'circuits.make_{circuit}')
    min_qubits = config.get('min-qubits', 6)
    shots = config.get('shots', 1000)
    trials = config.get('trials', 3)
    timeout_seconds = config.get('timeout-seconds', 600)
    max_memory_mb = config.get('max-memory-mb', 28_672)
    min_iters_time_seconds = config.get('min-iters-time-seconds', 1)
    max_iters = config.get('max-iters', 1e9)

    results_dir = config.get('results-dir', 'results')
    experiment_name = config.get('experiment-name',
                                 datetime.now().strftime("%Y%m%d-%H%M%S"))

    experiment_path = os.path.join(results_dir, experiment_name)

    if os.path.exists(experiment_path):
        shutil.rmtree(experiment_path)

    os.makedirs(experiment_path)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    handler = logging.FileHandler(os.path.join(experiment_path, 'log.txt'))
    handler.setLevel(logging.INFO)
    handler.setFormatter(logging.Formatter('%(asctime)s | %(levelname)s | %(message)s'))
    logger.addHandler(handler)

    stdout_handler = logging.StreamHandler()
    stdout_handler.setLevel(logging.INFO)
    stdout_handler.setFormatter(logging.Formatter('%(asctime)s | %(levelname)s | %(message)s'))
    logger.addHandler(stdout_handler)

    logger.info(f'Experiment: {experiment_name}')
    logger.info(f'Circuit: {circuit}')
    logger.info(f'Results dir: {experiment_path}')

    frameworks, methods = zip(*[sim.split('_', maxsplit=1) for sim in config['sims']])
    device = config.get('device', 'cpu')
    num_threads = config.get('num-threads', 1)
    sim_classes = [eval(f'wrappers.{framework.capitalize()}SimWrapper') for framework in frameworks]

    sims = []
    for sim_class, method in zip(sim_classes, methods):
        try:
            sim = sim_class(method=method, device=device, num_threads=num_threads)
            sims.append(sim)
        except NotImplementedError as e:
            logger.warning(f'{sim_class.__name__} | {e}')

    logger.removeHandler(stdout_handler)

    results = benchmark(sims=sims,
                        circuit_factory=circuit_factory,
                        min_qubits=min_qubits,
                        shots=shots,
                        trials=trials,
                        timeout_seconds=timeout_seconds,
                        max_memory_mb=max_memory_mb,
                        min_iters_time_seconds=min_iters_time_seconds,
                        max_iters=max_iters,
                        logger=logger)

    for df, benchmark_name in zip(results, ['exec_time', 'mem_usage', 'max_qubits']):
        df.to_csv(os.path.join(experiment_path, f'{benchmark_name}.csv'))

    for df, benchmark_name in zip(results, ['exec_time', 'mem_usage', 'max_qubits']):
        eval(f'visualization.save_{benchmark_name}_plot')(df, os.path.join(experiment_path, f'{benchmark_name}.pdf'))
